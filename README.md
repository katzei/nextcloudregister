# nextcloudregister

Nextcloud register form without using a nextcloud app.

# installation

To install you need to:

* install flask and requests
* clone the repo in the /site directory
* copy the etc content in /etc
* link /etc/nextcloudregister/nextcloudregister.service to /etc/systemd/system/nextcloudregister.service
* configure nextcloudregister fot your nextcloud instance in the file /etc/nextcloudregister/config.ini

By default it will serve on port 9000